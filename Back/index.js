const express = require('express');
const app = express();
const router = express.Router();
// const config = require('./config/database');
const path = require('path');
const api = require('./routes/api')(router);
const cors = require('cors');
const bodyParser = require('body-parser');
const port = process.env.PORT || 8080;

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.use('/api', api);

app.get('*', function(req, res){
  res.sendFile(path.join(__dirname + '/public/index.html'));
});

app.listen(port, ()=>{
  console.log("app runing on port"+ port);
});