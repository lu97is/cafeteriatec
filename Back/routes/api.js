'use strict'
const promise = require('bluebird');
const options = {
    promiseLib: promise
};

const pgp = require('pg-promise')(options);
const connectionString = 'postgres://ikblbuhpnkjfoq:11df6e723607cde9579686930b5b87f832b63b699a9a2b711f91356db6df5609@ec2-54-235-160-57.compute-1.amazonaws.com:5432/ddvd9q1jhp1lvb'
const db = pgp(connectionString);

module.exports = (router) => {
    //actor
    router.get('/actors', (req, res, next) => {
        db.query('select * from client')
            .then(function (data) {
                res.status(200)
                    .json({
                        status: 'success',
                        data: data

                    });
            })
            .catch(function (err) {
                return next(err);
            });
    })

    router.post('/records', (req,res,next)=>{
        const { quantity, age, time, totalPayment, client } = req.body
        db.func('insert_request', [quantity,age,time,totalPayment, client])
        .then(function(){
            res.status(200).json({
                success: true,
                message: 'Solicitud Enviada!'
            })
        }).catch(function(err){
            return next(err)
        })
    })
    
    router.post('/user', (req,res,next)=>{
        db.func('insert_user', [req.body.username, req.body.usertype])
        .then(function(){
            res.status(200).json({
                success: true,
                message: 'Saved'
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.get('/user/:username', (req,res,next)=>{
        db.func('get_user', req.params.username)
        .then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.get('/records/:id', (req,res,next)=>{
        db.func('get_record', req.params.id)
        .then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.get('/recordsPending/:id', (req,res,next)=>{
        db.func('get_record_peding', req.params.id)
        .then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.get('/recordsPending', (req,res,next)=>{
        db.func('get_record_peding')
        .then(function(data){
            res.status(200).json({
                success: true,
                message: data
            })
        }).catch(function(err){
            return next(err)
        })
    })

    router.put('/records_update/:id', (req,res,next)=>{
        db.func('update_request', [req.params.id, req.body.response])
        .then(function(){
            res.status(200).json({
                success: true,
                message: 'Actualizado!'
            })
        }).catch(function(err){
            return next(err)
        })
    })

    return router;
}
